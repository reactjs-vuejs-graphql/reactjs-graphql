import React from 'react';
import './App.css';
import {useQuery} from '@apollo/react-hooks';
import gql from "graphql-tag";
import LaunchesList from './components/LaunchesList'
import {Container, Loader} from 'semantic-ui-react'

const GET_LAUNCHES = gql`
  {
    allLaunches {
      id
      site
      launch_success
      launch_year
      rocket {
        id
        name
        type
      }
      mission {
        name
        missionPatchSmall
        missionPatchLarge
      }
    }
  }`;

function App() {
  const {data, loading, error} = useQuery(GET_LAUNCHES);

  if (loading) return <Loader active inline='centered'/>;
  if (error) return <p>Error</p>;

  return (
      <Container text>
        <LaunchesList launches={data.allLaunches}/>
      </Container>
  );
}

export default App;
