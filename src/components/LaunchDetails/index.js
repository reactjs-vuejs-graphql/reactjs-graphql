import React from 'react'
import {withRouter} from 'react-router-dom';
import gql from "graphql-tag";
import {useQuery} from "@apollo/react-hooks";
import {Button, Card, Container, Header, Icon, Loader, Modal} from "semantic-ui-react";
import Moment from 'react-moment';
import RocketDetails from "../RocketDetails";
import LaunchpadDetails from "../LaunchpadDetails";

const LaunchDetails = props => {

    const id = props.match.params.id

    const {loading: launch_loading, data: launch} = useQuery(launchQuery, {
        variables: {id: id},
    });

    const {loading: rocket_loading, data: rocket} = useQuery(rocketQuery, {
        variables: {id: launch && launch.launch.rocket.id},
        skip: launch == null
    });

    const {loading: launchpad_loading, data: launchpad} = useQuery(launchpadQuery, {
        variables: {id: launch && launch.launch.launch_site_id},
        skip: launch == null
    });

    if (launch_loading || rocket_loading || launchpad_loading) return <Loader active inline='centered'/>
    return (
        <React.Fragment>
            <Header textAlign='center' size='large'>
                <Header.Content><span role="img"
                                      aria-label="rocket">🚀</span> {launch.launch.mission.name} mission <span
                    role="img" aria-label="rocket">🚀</span></Header.Content>
            </Header>
            <br/>
            <Container text>
                <Card fluid>
                    <Card.Content>
                        <Icon style={{float: "right"}} name='info' size="large"/>
                        <Card.Header>Details</Card.Header>
                        <Card.Description>
                            {launch.launch.details}
                        </Card.Description>
                    </Card.Content>
                </Card>
                <Card.Group itemsPerRow={3} stackable>
                    <Card>
                        <Card.Content>
                            <Icon style={{float: "right"}} name='calendar alternate outline' size="large"/>
                            <Card.Header>Launch date</Card.Header>
                            <Card.Description>
                                <Moment format="DD/MM/YYYY">{launch.launch.launch_date_utc}</Moment>
                            </Card.Description>
                        </Card.Content>
                    </Card>
                    <Card>
                        <Card.Content>
                            <Icon style={{float: "right"}} name='warehouse' size="large"/>
                            <Card.Header>Launchpad</Card.Header>
                            <Card.Description>
                                <Modal size="large" trigger={<Button>{launch.launch.launch_site}</Button>}>
                                    <Modal.Header>{launch.launch.launch_site}</Modal.Header>
                                    <Modal.Content>
                                        <LaunchpadDetails launchpad={launchpad.launchpad}/>
                                    </Modal.Content>
                                </Modal>
                            </Card.Description>
                        </Card.Content>
                    </Card>
                    <Card>
                        <Card.Content>
                            <Icon style={{float: "right"}} name='rocket' size="large"/>
                            <Card.Header>{launch.launch.rocket.name}</Card.Header>
                            <Card.Description>
                                <Modal size="mini" trigger={<Button>See details</Button>}>
                                    <Modal.Header>{launch.launch.rocket.name}</Modal.Header>
                                    <Modal.Content>
                                        <RocketDetails rocket={rocket.rocket}/>
                                    </Modal.Content>
                                </Modal>
                            </Card.Description>
                        </Card.Content>
                    </Card>
                </Card.Group>
            </Container>
        </React.Fragment>
    )


}

const launchQuery = gql`

    query GetLaunchById($id: ID!) {
        launch(id: $id) {
            id
            site
            launch_date_utc
            launch_site
            launch_site_id
            details
            rocket {
                id
                name
                type
            }
            mission {
                name
                missionPatchSmall
                missionPatchLarge
            }
        }
    }
`;

const rocketQuery = gql`

    query GetRocketById($id: String!) {
        rocket(id: $id) {
            wikipedia
            description
            rocket_id
        }
    }
`;

const launchpadQuery = gql`

    query GetLaunchpadById($id: String!) {
        launchpad(id: $id) {
            status
            details
            wikipedia
            attempted_launches
            successful_launches
            site_name_long
            vehicles_launched
            site_id
            location {
                name
                region
                longitude
                latitude
            }
        }
    }
`;

export default withRouter(LaunchDetails);
