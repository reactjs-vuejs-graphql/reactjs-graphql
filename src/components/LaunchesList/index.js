/* eslint-disable camelcase */
import React, {Component} from 'react'
import {Card, Checkbox, Container, Image, Input, Statistic} from 'semantic-ui-react'
import Icon from "semantic-ui-react/dist/commonjs/elements/Icon";

export default class LaunchesList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            onlySuccess: false,
            launches: this.props.launches,
            filteredLaunches: this.props.launches,
            q: ''
        };

        this.onChange = this.onChange.bind(this);
        this.filterList = this.filterList.bind(this);
        this.onChangeCheckbox = this.onChangeCheckbox.bind(this);
        this.handleClearClick = this.handleClearClick.bind(this);
    }

    onChange(event) {
        const q = event.target.value.toLowerCase();
        this.setState({q}, () => this.filterList(this.state.onlySuccess));
    }

    filterList(onlySuccess) {
        let launches = this.state.launches;
        let q = this.state.q;

        launches = launches.filter(function (launch) {
            return (launch.mission.name.toLowerCase().indexOf(q) !== -1 || launch.rocket.name.toLowerCase().indexOf(q) !== -1
                || launch.launch_year.toLowerCase().indexOf(q) !== -1) && (!onlySuccess || launch.launch_success === 'true');
        });
        this.setState({filteredLaunches: launches});
    }

    handleClearClick() {
        this.setState({q: ''});
        if (!this.state.onlySuccess) {
            this.setState({filteredLaunches: this.state.launches});
        } else {
            this.setState({filteredLaunches: this.state.launches.filter(launch => launch.launch_success === this.state.onlySuccess)});
        }
    }

    onChangeCheckbox() {
        const newOnlySuccessValue = !this.state.onlySuccess;
        this.setState({onlySuccess: newOnlySuccessValue});
        this.filterList(newOnlySuccessValue);
    }

    render() {
        const mapLaunchesToItems = launches =>
            launches.map((launch) => {
                return {
                    href: `/launch/${launch.id}/`,
                    childKey: launch.id,
                    image: (
                        <Image src={launch.mission.missionPatchSmall ? launch.mission.missionPatchSmall : '/no_image.png'} size='medium' centered={true}/>
                    ),
                    header: launch.mission.name,
                    meta:
                        <Card.Meta style={{color: 'dimgray'}}>
                            {launch.launch_success === 'true' ?
                                <p><Icon color='green' name='check'/> Success</p> :
                                launch.launch_success === 'false' ?
                                <p><Icon color='red' name='close'/> Failure</p> :
                                <p><Icon name='calendar alternate outline'/> Upcoming</p>}
                            <p>Rocket: {launch.rocket.name}</p>
                            <p>Year: {launch.launch_year}</p>
                        </Card.Meta>,
                }
            })

        return (
            <React.Fragment>
                <Container>
                    <Input style={{"marginBottom": "10px"}} size='small' iconPosition='left' action={{
                        icon: "close",
                        onClick: () => this.handleClearClick()
                    }} icon='search' placeholder='Search launch...' value={this.state.q} onChange={this.onChange}/>
                    <Statistic style={{margin: "0", transform: "translate(20px,-8px)"}} size="mini">
                        <Statistic.Value>{this.state.filteredLaunches.length}</Statistic.Value>
                        <Statistic.Label>Launches</Statistic.Label>
                    </Statistic>
                    <Checkbox style={{float: "right", transform: "translateY(50%)"}} toggle label='Only success'
                              onChange={this.onChangeCheckbox}/>
                </Container>
                <Card.Group items={mapLaunchesToItems(this.state.filteredLaunches)} itemsPerRow={4} stackable/>
            </React.Fragment>
        )
    }

}
