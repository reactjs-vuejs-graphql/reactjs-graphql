import {Grid, Header, Icon, Image, List, Statistic} from "semantic-ui-react";
import React from "react";
import MapContainer from "../MapContainer";

const LaunchpadDetails = launchpad => {
    return (
        <Grid divided='vertically'>
            <Grid.Row columns={2}>
                <Grid.Column textAlign='center'>
                    <Header as='h2'>{launchpad.launchpad.location.name}</Header>
                    <Image centered={true} src={`/${launchpad.launchpad.site_id}.jpg`} style={{height: "250px"}}/>
                </Grid.Column>
                <Grid.Column textAlign='center'>
                    <Header as='h2'>Location</Header>
                    <MapContainer latitude={launchpad.launchpad.location.latitude} longitude={launchpad.launchpad.location.longitude}/>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
                <Grid.Column textAlign='center'>
                    <Header as='h2'>Details</Header>
                    {launchpad.launchpad.details}
                </Grid.Column>
                <Grid.Column textAlign='center'>
                    <Header as='h2'>More details</Header>
                    <a href={launchpad.launchpad.wikipedia} target="_blank" rel="noopener noreferrer">Wikipedia Link</a>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={3}>
                <Grid.Column textAlign='center'>
                    {launchpad.launchpad.status === 'active' && <Statistic><Statistic.Value>
                        <Icon name='check'/>
                    </Statistic.Value>
                        <Statistic.Label>Active</Statistic.Label></Statistic>}
                    {launchpad.launchpad.status === 'retired' && <Statistic><Statistic.Value>
                        <Icon name='close'/>
                    </Statistic.Value>
                        <Statistic.Label>Retired</Statistic.Label></Statistic>}
                    {launchpad.launchpad.status === 'under construction' && <Statistic><Statistic.Value>
                        <Icon name='wait'/>
                    </Statistic.Value>
                        <Statistic.Label>Under construction</Statistic.Label></Statistic>}
                </Grid.Column>
                <Grid.Column textAlign='center'>
                    <Statistic>
                        <Statistic.Value>
                            <Icon name='rocket'/>{launchpad.launchpad.attempted_launches}
                        </Statistic.Value>
                        <Statistic.Label>Attempted launches</Statistic.Label>
                    </Statistic>
                </Grid.Column>
                <Grid.Column textAlign='center'>
                    <Statistic>
                        <Statistic.Value>
                            <Icon name='rocket'/>{launchpad.launchpad.successful_launches}
                        </Statistic.Value>
                        <Statistic.Label>Successful launches</Statistic.Label>
                    </Statistic>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={1}>
                <Grid.Column textAlign='center'>
                    <Header as='h2'>Rockets</Header>
                    <List>
                        {launchpad.launchpad.vehicles_launched.map((item, index) => (
                            <List.Item key={index}>
                                <List.Content>{item}</List.Content>
                            </List.Item>
                        ))}
                    </List>
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default LaunchpadDetails;
