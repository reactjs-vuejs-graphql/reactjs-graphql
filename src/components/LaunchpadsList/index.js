import React from "react";
import {Loader, Tab} from "semantic-ui-react";
import gql from "graphql-tag";
import {withRouter} from "react-router-dom";
import {useQuery} from "@apollo/react-hooks";
import LaunchpadDetails from "../LaunchpadDetails";

const LaunchpadsList = () => {

    const {loading: launchpads_loading, data: launchpads} = useQuery(launchpadsQuery);

    if (launchpads_loading) return <Loader active inline='centered'/>

    const panes = [];
    launchpads.launchpads.forEach((launchpad, index) =>
        panes.push({
            key: {index}, menuItem: launchpad.site_name_long, render: () =>
                <Tab.Pane style={{width: "950px"}}>
                    <LaunchpadDetails key={index} launchpad={launchpad}/>
                </Tab.Pane>
        })
    )

    return (
        <React.Fragment>
            <Tab menu={{fluid: true, vertical: true, tabular: true}} panes={panes}/>
        </React.Fragment>
    )
}

const launchpadsQuery = gql`

    query GetLaunchpads {
        launchpads {
            status
            details
            wikipedia
            attempted_launches
            successful_launches
            site_name_long
            vehicles_launched
            site_id
            location {
                name
                region
                latitude
                longitude
            }
        }
    }
`;

export default withRouter(LaunchpadsList);
