import React, {Fragment} from 'react';
import {Link, Route} from 'react-router-dom';
import {Header, Image, Menu} from "semantic-ui-react";

export default class LayoutDefault extends React.Component {

    render() {
        return (
            <Fragment>
                <div>
                    <Header as='h1' icon textAlign='center'>
                        <Image
                            href='/'
                            centered
                            src='/SpaceX.jpg'
                        />
                        <Header.Content>
                            <Fragment>
                                SpaceX Launches
                                <Menu text style={{
                                    display: "flex",
                                    justifyContent: "center"
                                }}>
                                    <Menu.Item
                                        as={Link} to='/launches'
                                        name='launches'
                                    />
                                    <Menu.Item
                                        as={Link} to='/rockets'
                                        name='rockets'
                                    />
                                    <Menu.Item
                                        as={Link} to='/launchpads'
                                        name='launchpads'
                                    />
                                </Menu>
                            </Fragment>
                        </Header.Content>
                    </Header>
                </div>
                <Route {...this.props} />
            </Fragment>
        )
    }
}
