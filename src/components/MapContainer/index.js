import {Map, GoogleApiWrapper, Marker} from 'google-maps-react';
import React from "react";
import {Component} from "react";

export class MapContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            lat: this.props.latitude, lng: this.props.longitude
        }
    }

    static getDerivedStateFromProps(nextProps){
        return({lat: nextProps.latitude, lng: nextProps.longitude});
    }

    displayMarkers = () => {
        return <Marker position={{
            lat: this.state.lat,
            lng: this.state.lng
        }} />
    }

    render() {
        const mapStyles = {
            width: '446px',
            height: '250px',
        };
        return (
            <Map
                google={this.props.google}
                zoom={8}
                style={mapStyles}
                initialCenter={{ lat: this.state.lat, lng: this.state.lng}}
            >
                {this.displayMarkers()}
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCoGhVNQDets9Je_5CGZYnDTWME9evBPm8'
})(MapContainer);
