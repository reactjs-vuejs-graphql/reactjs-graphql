import {Card, Image} from "semantic-ui-react";
import React from "react";

const RocketDetails = rocket => {
    return (
        <Card centered>
            <Image src={`/${rocket.rocket.rocket_id}.jpg`}/>
            <Card.Content>
                <Card.Header>{rocket.rocket.rocket_name}</Card.Header>
                <Card.Meta><a href={rocket.rocket.wikipedia} target="_blank" rel="noopener noreferrer">Wikipedia
                    Link</a></Card.Meta>
                <Card.Description>{rocket.rocket.description}</Card.Description>
            </Card.Content>
        </Card>
    )
}

export default RocketDetails;
