import React from 'react'
import {withRouter} from 'react-router-dom';
import gql from "graphql-tag";
import {useQuery} from "@apollo/react-hooks";
import {Card, Container, Grid, Header, Icon, Image, Loader, Segment, Statistic, Tab} from "semantic-ui-react";
import RocketDetails from "../RocketDetails";

const RocketsList = () => {

    const {loading: rockets_loading, data: rockets} = useQuery(rocketQuery);

    if (rockets_loading) return <Loader active inline='centered'/>

    const panes = [];

    rockets.rockets.map((item, index) => (
        panes.push({
            key: {index}, menuItem: item.rocket_name, render: () =>
                <Tab.Pane style={{width: "950px"}}>
                    <Grid columns={3} divided>
                        <Grid.Row stretched>
                            <Grid.Column>
                                <Segment><Image src={`/${item.rocket_id}.jpg`}/></Segment>
                            </Grid.Column>
                            <Grid.Column textAlign='center' verticalAlign='middle'>
                                <Segment>{item.description}</Segment>
                                <Segment><a href={item.wikipedia} target="_blank" rel="noopener noreferrer">Wikipedia
                                    Link</a></Segment>
                            </Grid.Column>
                            <Grid.Column textAlign='center'>
                                <Segment>
                                    <Statistic>
                                        <Statistic.Value>{item.stages}</Statistic.Value>
                                        <Statistic.Label>Stages</Statistic.Label>
                                    </Statistic>
                                </Segment>
                                <Segment>
                                    <Statistic>
                                        <Statistic.Value>{item.boosters}</Statistic.Value>
                                        <Statistic.Label>Boosters</Statistic.Label>
                                    </Statistic>
                                </Segment>
                                <Segment>
                                    {item.active && <Statistic><Statistic.Value>
                                        <Icon name='check'/>
                                    </Statistic.Value>
                                        <Statistic.Label>Active</Statistic.Label></Statistic>}
                                    {!item.active && <Statistic><Statistic.Value>
                                        <Icon name='close'/>
                                    </Statistic.Value>
                                        <Statistic.Label>Retired</Statistic.Label></Statistic>}
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Tab.Pane>
        })
    ))

    return (
        <React.Fragment>
            <Header textAlign='center' size='large'>
                <Header.Content>Rockets Tabs Display</Header.Content>
            </Header>
            <Tab menu={{fluid: true, vertical: true, tabular: true}} panes={panes}/>
            <Header textAlign='center' size='large'>
                <Header.Content>Rockets Container Display</Header.Content>
            </Header>
            <Container text>
                <Card.Group itemsPerRow={2} stackable>
                    {rockets.rockets.map((item, index) => (
                        <RocketDetails rocket={item} key={index}/>
                    ))}
                </Card.Group>
            </Container>
        </React.Fragment>
    )


}

const rocketQuery = gql`
    {
        rockets {
            wikipedia
            description
            rocket_id
            rocket_name
            stages
            boosters
            active
        }
    }`;

export default withRouter(RocketsList);
