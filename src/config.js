const env = process.env.NODE_ENV;

const development = {
    api: 'http://localhost:4000/'
};

const production = {
    api: 'https://ytktc55fde.execute-api.eu-central-1.amazonaws.com/dev/graphql/'
};

const config = {
    development,
    production
};

module.exports = config[env];
