import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {HttpLink} from 'apollo-link-http';
import {ApolloProvider} from '@apollo/react-hooks';
import 'semantic-ui-css/semantic.min.css';
import {BrowserRouter, Switch, withRouter} from 'react-router-dom'
import LaunchDetails from './components/LaunchDetails'
import LayoutDefault from './components/Layout'
import App from "./App";
import RocketsList from "./components/RocketsList";
import LaunchpadsList from "./components/LaunchpadsList";
import config from "./config";

const cache = new InMemoryCache();
const link = new HttpLink({
    uri: config.api
})

const client = new ApolloClient({
    cache,
    link
})

const Root = () => {
    return (
        <BrowserRouter>
            <ApolloProvider client={client}>
                <Switch>
                    <LayoutDefault path="/launchpads" component={withRouter(LaunchpadsList)}/>
                    <LayoutDefault path="/rockets" component={withRouter(RocketsList)}/>
                    <LayoutDefault path="/launch/:id" component={withRouter(LaunchDetails)}/>
                    <LayoutDefault path="/" component={withRouter(App)}/>
                </Switch>
            </ApolloProvider>
        </BrowserRouter>
    )
}
ReactDOM.render(
    <Root/>,
    document.querySelector('#root')
);

serviceWorker.unregister();
